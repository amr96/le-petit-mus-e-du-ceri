package com.example.lepetitmuseduceri;

import androidx.appcompat.app.AppCompatActivity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

public class EquipementDetails extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_equipement_details);

        TextView name = (TextView)findViewById(R.id.Name);
        TextView brand = (TextView)findViewById(R.id.txtBrand);
        TextView categorie = (TextView)findViewById(R.id.txtCat);
        TextView year = (TextView)findViewById(R.id.txtYear);
        TextView description = (TextView)findViewById(R.id.txtDesc);
        TextView details = (TextView)findViewById(R.id.txtDetails);

        String nameValue = getIntent().getStringExtra ("name");
        String brandValue = getIntent().getStringExtra ("marque");
        String yearValue = getIntent().getStringExtra ("year");
        String descriptionValue = getIntent().getStringExtra ("description");



        String categorieValue = getIntent().getStringExtra("categorie");
         String detailsValue = getIntent().getStringExtra ("details");

        name.setText(nameValue);
        brand.setText(brandValue);
        year.setText(yearValue);
        description.setText(descriptionValue);
        categorie.setText(categorieValue);
        details.setText(detailsValue);



    }


}
