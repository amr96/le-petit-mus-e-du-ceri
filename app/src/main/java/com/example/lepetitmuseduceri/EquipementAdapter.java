package com.example.lepetitmuseduceri;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class EquipementAdapter extends ArrayAdapter<Equipement> {
    private ArrayList<Equipement> items;

    private Context mContext;

    MainActivity activity;

    public EquipementAdapter(Context context, int textViewResourceID, ArrayList<Equipement> items){

        super(context,textViewResourceID,items);

        mContext = context;

        this.items = items;

    }

    @Override

    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        final Equipement equipement = items.get(position);

        if(v==null){

            LayoutInflater inflater =(LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            v=inflater.inflate(R.layout.item,null);

        }

        TextView name = (TextView)v.findViewById(R.id.textView3);
        TextView brand = (TextView)v.findViewById(R.id.textBrand);
        TextView categorie = (TextView)v.findViewById(R.id.textCategories);

        if (name != null) {

            name.setText("Name: "+ equipement.getName());

        }

        if (brand != null) {

            brand.setText("Marque:"+equipement.getBrand());

        }

        if (categorie != null) {
            String res= "Catégories: ";

            for (int i=0;i<equipement.getCategories().size();i++){
                res +=  equipement.getCategories().get(i) + ", ";

            }
            categorie.setText(res);
        }





        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, EquipementDetails.class);
                intent.putExtra("name",equipement.getName());

                intent.putExtra("marque",equipement.getBrand());
                intent.putExtra("year",equipement.getYear());
                intent.putExtra("description",equipement.getDescription());
                intent.putExtra("details",equipement.getTechnicalDetails());

                String cate= "";
                for (int i=0;i<equipement.getCategories().size();i++) {
                    cate += equipement.getCategories().get(i) + ", ";

                }
                intent.putExtra("categorie",cate);

                String details= "";
                for (int i=0;i<equipement.getTechnicalDetails().size();i++) {
                    details += equipement.getTechnicalDetails().get(i) + ", ";

                }
                intent.putExtra("details",details);

                mContext.startActivity(intent);

            }
        });

        return v;

    }



}
