package com.example.lepetitmuseduceri;

import androidx.appcompat.app.AppCompatActivity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

public class MainActivity extends AppCompatActivity {

    ArrayList<Equipement> EquipementList = new ArrayList<Equipement>();

    String url ="https://demo-lia.univ-avignon.fr/cerimuseum/catalog";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        new BackTask().execute(url);

        ListView lv = (ListView) findViewById(R.id.listView);
        EditText inputSearch = (EditText) findViewById(R.id.txtSearch);


    }

    public class BackTask extends AsyncTask<String,String,String> {

        @Override

        protected void onPreExecute() {

            super.onPreExecute();

        }

        @Override

        protected String doInBackground(String... strings) {

            String content = HttpULRConnect.getData(url);

            Log.d("content",content);

            return content;

        }

        @Override

        protected void onPostExecute(String s) {
            Log.d("s",s);
            if(s!=null){


                String json=s;
                try{
                    JSONObject jObject = new JSONObject(json.trim());
                    Iterator<?> keys = jObject.keys();

                    while( keys.hasNext() ) {
                        String key = (String)keys.next();
                        Log.d("Key",key);
                        if ( jObject.get(key) instanceof JSONObject ) {

                            Equipement  equipement = new Equipement();

                            //id
                            equipement.setId(key);

                            //name
                            equipement.setName(((JSONObject) jObject.get(key)).getString("name"));

                            //categorie
                            JSONArray ar = ((JSONObject) jObject.get(key)).getJSONArray("categories");

                            ArrayList<String> Categories=new ArrayList<>();
                            for (int i=0; i< ar.length(); i++) {

                                Categories.add(ar.getString(i));
                            }
                            equipement.setCategories(Categories);

                            //Description
                            equipement.setDescription(((JSONObject) jObject.get(key)).getString("description"));
                            Log.d("description",((JSONObject) jObject.get(key)).getString("description"));

                            //year
                            if (((JSONObject) jObject.get(key)).has("year")) {
                                equipement.setYear(((JSONObject) jObject.get(key)).getString("year"));
                                Log.d("year",((JSONObject) jObject.get(key)).getString("year"));
                            }else
                            {
                                equipement.setYear("Non mentionné");
                            }

                            //brand
                            if (((JSONObject) jObject.get(key)).has("brand")) {

                                Log.d("brand",((JSONObject) jObject.get(key)).getString("brand"));
                                equipement.setBrand(((JSONObject) jObject.get(key)).getString("brand"));
                            }
                            else
                            {
                                equipement.setBrand("Non mentionné");
                            }


                            //technicalDetails
                            ArrayList<String> technicalDetails=new ArrayList<>();

                            if (((JSONObject) jObject.get(key)).has("technicalDetails")) {
                                JSONArray technicalDetailsArrays= ((JSONObject) jObject.get(key)).getJSONArray("technicalDetails");
                                for (int i=0; i< technicalDetailsArrays.length(); i++) {

                                    technicalDetails.add(technicalDetailsArrays.getString(i));
                                }
                            }else{
                                technicalDetails.add("Non mentionné");
                            }
                            equipement.setTechnicalDetails(technicalDetails);





                            EquipementList.add(equipement);

                        }
                    }
                }catch (JSONException e){

                    Log.d("error","errooor");
                    e.printStackTrace();

                }


                EquipementAdapter adapter = new EquipementAdapter(MainActivity.this, R.layout.item, EquipementList);

                ListView lv = (ListView) findViewById(R.id.listView);

                lv.setAdapter(adapter);
            }
            else
            {
                Toast.makeText(getApplicationContext(),"rien",Toast.LENGTH_SHORT).show();
            }




        }

    }
}
