package com.example.lepetitmuseduceri;

import java.util.ArrayList;

public class Equipement {

    private String id;
    private String name;
    private String brand;
    private String year;
    private String description;
    private ArrayList<String> Categories;
    private ArrayList<String> technicalDetails;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public ArrayList<String> getCategories() {
        return Categories;
    }

    public void setCategories(ArrayList<String> categories) {
        Categories = categories;
    }

    public ArrayList<String> getTechnicalDetails() {
        return technicalDetails;
    }

    public void setTechnicalDetails(ArrayList<String> technicalDetails) {
        this.technicalDetails = technicalDetails;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
